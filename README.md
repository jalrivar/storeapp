/*********************************************/
Proyecto StoreApp
/*********************************************/
/*********************************************/
Requisitos
/*********************************************/

Microsoft SQL Sever 2016
.Net Framework 4.7.2

/*********************************************/
Pasos para instalación
/*********************************************/
SQL Server:

1. Creación de base de datos con el nombre StoreApp.
2. Creación de base de datos con el nombre SecurityApp.
3. Creación de login y usuario de base de datos (autenticación SQL) con permisos db_owner sobre la base de datos StoreApp y SecurityApp.
4. Ejecución del script SQL https://gitlab.com/jalrivar/storeapp/blob/master/SQL%20Scripts/StoreAppSqlScripts.sql para la creación de objetos de la base de datos.
5. Ejecución del script SQL https://gitlab.com/jalrivar/storeapp/blob/master/SQL%20Scripts/StoreSecuritySqlScripts.sql

IIS:

1. Publicar el proyecto Store.API de la solución https://gitlab.com/jalrivar/storeapp/blob/master/StoreApp.sln
2. Configurar servidor, base de datos, usuario y clave en las cadenas de conexión "Entities" y "SecurityStoreEntities" en el web.config del Web API publicado.

/*********************************************/
Colección de Postman para pruebas
/*********************************************/
Disponible desde: https://gitlab.com/jalrivar/storeapp/blob/master/Postman%20Collection/StoreApp.postman_collection.json