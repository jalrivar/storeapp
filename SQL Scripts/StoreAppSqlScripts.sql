USE SnackStore
GO
        
SET NOCOUNT ON;

/********************************************************************************************************************************************************************************************/
--Creacin de esquema.
/********************************************************************************************************************************************************************************************/

IF NOT EXISTS (SELECT 'X' FROM sys.schemas WHERE [name] = 'Store')
BEGIN
	EXEC('CREATE SCHEMA Store')
END
GO

/********************************************************************************************************************************************************************************************/
--Creacin de tablas.
/********************************************************************************************************************************************************************************************/

DROP TABLE IF EXISTS Store.SalesDetails;
DROP TABLE IF EXISTS Store.Sales;
DROP TABLE IF EXISTS Store.Customers;
DROP TABLE IF EXISTS Store.Vendors;
DROP TABLE IF EXISTS Store.Movements;
DROP TABLE IF EXISTS Store.Products;
GO

CREATE TABLE Store.Products
(
	ProductId INT PRIMARY KEY NOT NULL IDENTITY(1, 1),
	ProductCode VARCHAR(50) NOT NULL,
	ProductName VARCHAR(100) NOT NULL,
	ProductStateId INT NOT NULL,
	Stock DECIMAL (19, 5) NOT NULL,
	SalePrice DECIMAL (19,5) NOT NULL,
	CostPrice DECIMAL (19,5) NOT NULL,
	Likes INT NOT NULL,
	CONSTRAINT Unique_ProductCode UNIQUE(ProductCode)
);

CREATE TABLE Store.Movements
(
	MovementId INT PRIMARY KEY NOT NULL IDENTITY(1, 1),
	MovementName VARCHAR(25) NOT NULL,
	MovementDate DATETIME NOT NULL,
	ProductId INT NOT NULL,
	Stock DECIMAL (19, 5) NOT NULL,
	SalePrice DECIMAL (19,5) NOT NULL,
	CostPrice DECIMAL (19,5) NOT NULL,
	CreatedBy INT NOT NULL,
	CONSTRAINT FK_Movements_Products FOREIGN KEY (ProductId) REFERENCES Store.Products (ProductId)
);
GO

CREATE TABLE Store.Customers
(
	CustomerId INT PRIMARY KEY NOT NULL IDENTITY(1, 1),
	CustomerName VARCHAR(100) NOT NULL
);

CREATE TABLE Store.Vendors
(
	VendorId INT PRIMARY KEY NOT NULL IDENTITY(1, 1),
	VendorName VARCHAR(100) NOT NULL
);

CREATE TABLE Store.Sales
(
	SaleId INT PRIMARY KEY NOT NULL IDENTITY (1, 1),
	CustomerId INT NOT NULL,
	VendorId INT NOT NULL,
	InvoiceNumber VARCHAR(25) NOT NULL,
	SaleDate DATETIME NOT NULL,
	CONSTRAINT Unique_InvoiceNumber UNIQUE(InvoiceNumber),
	CONSTRAINT FK_Sales_Customers FOREIGN KEY (CustomerId) REFERENCES Store.Customers (CustomerId),
	CONSTRAINT FK_Sales_Vendors FOREIGN KEY (VendorId) REFERENCES Store.Vendors (VendorId)
);

CREATE TABLE Store.SalesDetails
(
	SaleId INT NOT NULL,
	ProductId INT NOT NULL,
	Quantity DECIMAL (19,5) NOT NULL,
	SalePrice DECIMAL (19,5) NOT NULL,
	PRIMARY KEY (SaleId, ProductId),
	CONSTRAINT FK_SD_Sales FOREIGN KEY (SaleId) REFERENCES Store.Sales (SaleId),
	CONSTRAINT FK_SD_Products FOREIGN KEY (ProductId) REFERENCES Store.Products (ProductId)
);
GO

/********************************************************************************************************************************************************************************************/
--Creacin de procedimientos.
/********************************************************************************************************************************************************************************************/

IF OBJECT_ID('Store.SpVendorsSelect') IS NOT NULL
BEGIN
	DROP PROCEDURE Store.SpVendorsSelect
END
GO
CREATE PROCEDURE Store.SpVendorsSelect
		@VendorId [int]
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	SELECT [VendorId], [VendorName]
	FROM [Store].[Vendors]
	WHERE ([VendorId] = SCOPE_IDENTITY())

	COMMIT
GO
IF OBJECT_ID('Store.SpVendorsInsert') IS NOT NULL
BEGIN
	DROP PROCEDURE Store.SpVendorsInsert
END
GO
CREATE PROCEDURE Store.SpVendorsInsert
	(
		@VendorName [varchar](100)
	)
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	INSERT INTO [Store].[Vendors]
	(
		[VendorName]
	)
	VALUES
	(
		@VendorName

	)
	SELECT [VendorId], [VendorName]
	FROM [Store].[Vendors]
	WHERE ([VendorId] = SCOPE_IDENTITY())

	COMMIT
GO
IF OBJECT_ID('Store.SpVendorsUpdate') IS NOT NULL
BEGIN
	DROP PROCEDURE Store.SpVendorsUpdate
END
GO
CREATE PROCEDURE Store.SpVendorsUpdate
	(
		@VendorId [int],
		@VendorName [varchar](100)
	)
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION
		UPDATE [Store].[Vendors]
		SET [VendorName] = @VendorName
		WHERE ([VendorId] = SCOPE_IDENTITY())
	COMMIT
GO
IF OBJECT_ID('Store.SpVendorsDelete') IS NOT NULL
BEGIN
	DROP PROCEDURE Store.SpVendorsDelete
END
GO
CREATE PROCEDURE Store.SpVendorsDelete
		@VendorId [int]
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION
		DELETE FROM [Store].[Vendors]
		WHERE ([VendorId] = SCOPE_IDENTITY())
	COMMIT
GO
IF OBJECT_ID('Store.SpCustomersSelect') IS NOT NULL
BEGIN
	DROP PROCEDURE Store.SpCustomersSelect
END
GO
CREATE PROCEDURE Store.SpCustomersSelect
		@CustomerId [int]
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	SELECT [CustomerId], [CustomerName]
	FROM [Store].[Customers]
	WHERE ([CustomerId] = SCOPE_IDENTITY())

	COMMIT
GO
IF OBJECT_ID('Store.SpCustomersInsert') IS NOT NULL
BEGIN
	DROP PROCEDURE Store.SpCustomersInsert
END
GO
CREATE PROCEDURE Store.SpCustomersInsert
	(
		@CustomerName [varchar](100)
	)
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	INSERT INTO [Store].[Customers]
	(
		[CustomerName]
	)
	VALUES
	(
		@CustomerName

	)
	SELECT [CustomerId], [CustomerName]
	FROM [Store].[Customers]
	WHERE ([CustomerId] = SCOPE_IDENTITY())

	COMMIT
GO
IF OBJECT_ID('Store.SpCustomersUpdate') IS NOT NULL
BEGIN
	DROP PROCEDURE Store.SpCustomersUpdate
END
GO
CREATE PROCEDURE Store.SpCustomersUpdate
	(
		@CustomerId [int],
		@CustomerName [varchar](100)
	)
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION
		UPDATE [Store].[Customers]
		SET [CustomerName] = @CustomerName
		WHERE ([CustomerId] = SCOPE_IDENTITY())
	COMMIT
GO
IF OBJECT_ID('Store.SpCustomersDelete') IS NOT NULL
BEGIN
	DROP PROCEDURE Store.SpCustomersDelete
END
GO
CREATE PROCEDURE Store.SpCustomersDelete
		@CustomerId [int]
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION
		DELETE FROM [Store].[Customers]
		WHERE ([CustomerId] = SCOPE_IDENTITY())
	COMMIT
GO
IF OBJECT_ID('Store.SpProductsSelect') IS NOT NULL
BEGIN
	DROP PROCEDURE Store.SpProductsSelect
END
GO
CREATE PROCEDURE Store.SpProductsSelect
		@ProductId [int]
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	SELECT [ProductId], [ProductCode], [ProductName], [ProductStateId], [Stock], [SalePrice], [CostPrice]
	FROM [Store].[Products]
	WHERE ([ProductId] = SCOPE_IDENTITY())

	COMMIT
GO
IF OBJECT_ID('Store.SpProductsInsert') IS NOT NULL
BEGIN
	DROP PROCEDURE Store.SpProductsInsert
END
GO
CREATE PROCEDURE Store.SpProductsInsert
	(
		@ProductCode [varchar](50),
		@ProductName [varchar](100),
		@ProductStateId [int],
		@Stock [decimal](19, 5),
		@SalePrice [decimal](19, 5),
		@CostPrice [decimal](19, 5)
	)
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	INSERT INTO [Store].[Products]
	(
		[ProductCode], [ProductName], [ProductStateId], [Stock], [SalePrice], [CostPrice]
	)
	VALUES
	(
		@ProductCode,
		@ProductName,
		@ProductStateId,
		@Stock,
		@SalePrice,
		@CostPrice

	)
	SELECT [ProductId], [ProductCode], [ProductName], [ProductStateId], [Stock], [SalePrice], [CostPrice]
	FROM [Store].[Products]
	WHERE ([ProductId] = SCOPE_IDENTITY())

	COMMIT
GO
IF OBJECT_ID('Store.SpProductsUpdate') IS NOT NULL
BEGIN
	DROP PROCEDURE Store.SpProductsUpdate
END
GO
CREATE PROCEDURE Store.SpProductsUpdate
	(
		@ProductId [int],
		@ProductCode [varchar](50),
		@ProductName [varchar](100),
		@ProductStateId [int],
		@Stock [decimal](19, 5),
		@SalePrice [decimal](19, 5),
		@CostPrice [decimal](19, 5)
	)
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION
		UPDATE [Store].[Products]
		SET [ProductCode] = @ProductCode, [ProductName] = @ProductName, [ProductStateId] = @ProductStateId, [Stock] = @Stock, [SalePrice] = @SalePrice, [CostPrice] = @CostPrice
		WHERE ([ProductId] = SCOPE_IDENTITY())
	COMMIT
GO
IF OBJECT_ID('Store.SpProductsDelete') IS NOT NULL
BEGIN
	DROP PROCEDURE Store.SpProductsDelete
END
GO
CREATE PROCEDURE Store.SpProductsDelete
		@ProductId [int]
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION
		DELETE FROM [Store].[Products]
		WHERE ([ProductId] = SCOPE_IDENTITY())
	COMMIT
GO
IF OBJECT_ID('Store.SpSalesSelect') IS NOT NULL
BEGIN
	DROP PROCEDURE Store.SpSalesSelect
END
GO
CREATE PROCEDURE Store.SpSalesSelect
		@SaleId [int]
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	SELECT [SaleId], [CustomerId], [VendorId], [InvoiceNumber], [SaleDate]
	FROM [Store].[Sales]
	WHERE ([SaleId] = SCOPE_IDENTITY())

	COMMIT
GO
IF OBJECT_ID('Store.SpSalesInsert') IS NOT NULL
BEGIN
	DROP PROCEDURE Store.SpSalesInsert
END
GO
CREATE PROCEDURE Store.SpSalesInsert
	(
		@CustomerId [int],
		@VendorId [int],
		@InvoiceNumber [varchar](25),
		@SaleDate [datetime]
	)
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	INSERT INTO [Store].[Sales]
	(
		[CustomerId], [VendorId], [InvoiceNumber], [SaleDate]
	)
	VALUES
	(
		@CustomerId,
		@VendorId,
		@InvoiceNumber,
		@SaleDate

	)
	SELECT [SaleId], [CustomerId], [VendorId], [InvoiceNumber], [SaleDate]
	FROM [Store].[Sales]
	WHERE ([SaleId] = SCOPE_IDENTITY())

	COMMIT
GO
IF OBJECT_ID('Store.SpSalesUpdate') IS NOT NULL
BEGIN
	DROP PROCEDURE Store.SpSalesUpdate
END
GO
CREATE PROCEDURE Store.SpSalesUpdate
	(
		@SaleId [int],
		@CustomerId [int],
		@VendorId [int],
		@InvoiceNumber [varchar](25),
		@SaleDate [datetime]
	)
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION
		UPDATE [Store].[Sales]
		SET [CustomerId] = @CustomerId, [VendorId] = @VendorId, [InvoiceNumber] = @InvoiceNumber, [SaleDate] = @SaleDate
		WHERE ([SaleId] = SCOPE_IDENTITY())
	COMMIT
GO
IF OBJECT_ID('Store.SpSalesDelete') IS NOT NULL
BEGIN
	DROP PROCEDURE Store.SpSalesDelete
END
GO
CREATE PROCEDURE Store.SpSalesDelete
		@SaleId [int]
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION
		DELETE FROM [Store].[Sales]
		WHERE ([SaleId] = SCOPE_IDENTITY())
	COMMIT
GO
IF OBJECT_ID('Store.SpMovementsSelect') IS NOT NULL
BEGIN
	DROP PROCEDURE Store.SpMovementsSelect
END
GO
CREATE PROCEDURE Store.SpMovementsSelect
		@MovementId [int]
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	SELECT [MovementId], [MovementName], [MovementDate], [ProductId], [Stock], [SalePrice], [CostPrice], [CreatedBy]
	FROM [Store].[Movements]
	WHERE ([MovementId] = SCOPE_IDENTITY())

	COMMIT
GO
IF OBJECT_ID('Store.SpMovementsInsert') IS NOT NULL
BEGIN
	DROP PROCEDURE Store.SpMovementsInsert
END
GO
CREATE PROCEDURE Store.SpMovementsInsert
	(
		@MovementName [varchar](25),
		@MovementDate [datetime],
		@ProductId [int],
		@Stock [decimal](19, 5),
		@SalePrice [decimal](19, 5),
		@CostPrice [decimal](19, 5),
		@CreatedBy [int]
	)
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	INSERT INTO [Store].[Movements]
	(
		[MovementName], [MovementDate], [ProductId], [Stock], [SalePrice], [CostPrice], [CreatedBy]
	)
	VALUES
	(
		@MovementName,
		@MovementDate,
		@ProductId,
		@Stock,
		@SalePrice,
		@CostPrice,
		@CreatedBy

	)
	SELECT [MovementId], [MovementName], [MovementDate], [ProductId], [Stock], [SalePrice], [CostPrice], [CreatedBy]
	FROM [Store].[Movements]
	WHERE ([MovementId] = SCOPE_IDENTITY())

	COMMIT
GO
IF OBJECT_ID('Store.SpMovementsUpdate') IS NOT NULL
BEGIN
	DROP PROCEDURE Store.SpMovementsUpdate
END
GO
CREATE PROCEDURE Store.SpMovementsUpdate
	(
		@MovementId [int],
		@MovementName [varchar](25),
		@MovementDate [datetime],
		@ProductId [int],
		@Stock [decimal](19, 5),
		@SalePrice [decimal](19, 5),
		@CostPrice [decimal](19, 5),
		@CreatedBy [int]
	)
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION
		UPDATE [Store].[Movements]
		SET [MovementName] = @MovementName, [MovementDate] = @MovementDate, [ProductId] = @ProductId, [Stock] = @Stock, [SalePrice] = @SalePrice, [CostPrice] = @CostPrice, [CreatedBy] = @CreatedBy
		WHERE ([MovementId] = SCOPE_IDENTITY())
	COMMIT
GO
IF OBJECT_ID('Store.SpMovementsDelete') IS NOT NULL
BEGIN
	DROP PROCEDURE Store.SpMovementsDelete
END
GO
CREATE PROCEDURE Store.SpMovementsDelete
		@MovementId [int]
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION
		DELETE FROM [Store].[Movements]
		WHERE ([MovementId] = SCOPE_IDENTITY())
	COMMIT
GO
