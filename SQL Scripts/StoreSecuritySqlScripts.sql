USE SecurityStore
GO

/********************************************************************************************************************************************************************************************/
--Creaci�n de tablas.
/********************************************************************************************************************************************************************************************/

IF OBJECT_ID ('[User]', 'U') IS NOT NULL
	DROP TABLE [User];
GO

CREATE TABLE [User]
(UserId     INT NOT NULL PRIMARY KEY IDENTITY(1, 1), 
 Username   CHAR(15) NOT NULL UNIQUE, 
 [Password] BINARY(20) NOT NULL, 
 Firstname  VARCHAR(75) NOT NULL, 
 Lastname   VARCHAR(75) NOT NULL, 
 Email      VARCHAR(150) NOT NULL, 
 IsDisabled BIT NOT NULL,
 UserRole   VARCHAR(255) NULL, 
 CreatedBy  CHAR(15) NOT NULL, 
 CreatedOn  DATETIME DEFAULT GETDATE() NOT NULL, 
 ModifiedBy CHAR(15) NULL, 
 ModifiedOn DATETIME NULL
);
GO

/********************************************************************************************************************************************************************************************/
--Creaci�n de procedimientos.
/********************************************************************************************************************************************************************************************/

IF OBJECT_ID('[SpInsertUser]', 'P') IS NOT NULL
    DROP PROCEDURE [SpInsertUser];
GO
CREATE PROCEDURE [SpInsertUser]
(@Username   CHAR(15)     = '', 
 @Password   BINARY(20)   = NULL, 
 @Firstname  VARCHAR(25)  = '', 
 @Lastname   VARCHAR(25)  = '', 
 @Email      VARCHAR(150) = '', 
 @IsDisabled BIT          = 'false', 
 @UserRole   VARCHAR(255) = '',
 @CreatedBy  CHAR(15)     = ''
)
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        INSERT INTO [User]
        ([Username], 
         [Password], 
         [Firstname], 
         [Lastname], 
         [Email], 
         [IsDisabled], 
		 [UserRole],
         [CreatedBy], 
         [CreatedOn]
        )
        VALUES
        (@Username, 
         HASHBYTES('SHA', HASHBYTES('MD5', @Password)), 
         @Firstname, 
         @Lastname, 
         @Email, 
         0, 
		 @UserRole,
         @CreatedBy, 
         GETDATE()
        );
        SELECT SCOPE_IDENTITY() AS UserId;
    END;
GO

IF OBJECT_ID ('[SpUpdateUser]', 'P') IS NOT NULL
	DROP PROCEDURE [SpUpdateUser];
GO

CREATE PROCEDURE [SpUpdateUser]
(@UserId     INT          = 0, 
 @Username   CHAR(15)     = '', 
 @Password   BINARY(20)   = NULL, 
 @Firstname  VARCHAR(25)  = '', 
 @Lastname   VARCHAR(25)  = '', 
 @Email      VARCHAR(150) = '', 
 @IsDisabled BIT          = 'false', 
 @UserRole   VARCHAR(255) = '',
 @ModifiedBy CHAR(15)     = ''
)
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;

        UPDATE [user]
          SET 
              [Username] = @Username, 
              [Password] = CASE
                               WHEN @Password IS NULL
                               THEN [Password]
                               ELSE HASHBYTES('SHA', HASHBYTES('MD5', @Password))
                           END, 
              [Firstname] = @Firstname, 
              [Lastname] = @Lastname, 
              [Email] = @Email, 
              [IsDisabled] = @IsDisabled, 
			  [UserRole] = @UserRole,
              [ModifiedBy] = @ModifiedBy, 
              [ModifiedOn] = GETDATE()
        WHERE UserId = @UserId;

    END;
GO

IF OBJECT_ID ('[SpDeleteUser]', 'P') IS NOT NULL
	DROP PROCEDURE [SpDeleteUser];
GO

CREATE PROCEDURE [SpDeleteUser](@UserId INT = 0)
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;

        DELETE FROM [User]
        WHERE UserId = @UserId;

    END;
GO


IF OBJECT_ID('[SpGetUser]', 'P') IS NOT NULL
    DROP PROCEDURE SpGetUser;
GO
CREATE PROCEDURE SpGetUser(@UserId INT = 0)
AS
    BEGIN
        SELECT [UserId], 
               [Username], 
               [Password], 
               [Firstname], 
               [Lastname], 
               [Email], 
               [IsDisabled], 
			   [UserRole],
               [CreatedBy], 
               [CreatedOn], 
               [ModifiedBy], 
               [ModifiedOn]
        FROM [User]
        WHERE UserId = @UserId
              OR @UserId = 0;
    END;
GO


IF OBJECT_ID('SpSetUserPassword', 'P') IS NOT NULL
    DROP PROCEDURE SpSetUserPassword;
GO
CREATE PROCEDURE SpSetUserPassword
(@UserName    CHAR(15)    = '', 
 @OldPassword VARCHAR(25) = NULL, 
 @NewPassword VARCHAR(25) = NULL
)
AS
    BEGIN
        DECLARE @RESULT_CODE INT= 0;
        DECLARE @RESULT_MESSAGE VARCHAR(255)= 'OK';
        
		SET NOCOUNT ON;

		IF EXISTS (SELECT 'X' FROM [User] WHERE Username = @Username AND [Password] = HASHBYTES('SHA', HASHBYTES('MD5', @OldPassword)))
		BEGIN
            SET @RESULT_CODE = 0;
			UPDATE A
			  SET 
				  A.[PASSWORD] = HASHBYTES('SHA', HASHBYTES('MD5', @NewPassword))
			FROM [User] A
			WHERE A.UserName = @UserName;
		END
	    ELSE
        BEGIN
            SET @RESULT_CODE = 1;
            SET @RESULT_MESSAGE = 'User or password not valid.';
        END;

        SELECT @RESULT_CODE AS RESULT_CODE, @RESULT_MESSAGE AS RESULT_MESSAGE;
    END;
GO

IF OBJECT_ID ('[SpValidateUser]', 'P') IS NOT NULL
	DROP PROCEDURE [SpValidateUser];
GO

CREATE PROCEDURE [SpValidateUser]
(@Username CHAR(15)    = '', 
 @Password VARCHAR(25) = NULL
)
AS
    BEGIN
        DECLARE @RESULT_CODE INT= 0;
        DECLARE @RESULT_MESSAGE VARCHAR(255)= 'OK';

        SET NOCOUNT ON;

        IF EXISTS (SELECT 'X' FROM [User] WHERE Username = @Username AND [Password] = HASHBYTES('SHA', HASHBYTES('MD5', @Password)))
		BEGIN
            SET @RESULT_CODE = 0;
		END
	    ELSE
        BEGIN
            SET @RESULT_CODE = 1;
            SET @RESULT_MESSAGE = 'User or password not valid.';
        END;

        SELECT @RESULT_CODE AS RESULT_CODE, @RESULT_MESSAGE AS RESULT_MESSAGE;
    END;
GO

exec SpValidateUser 'operator', 'operator'

select * from [User]