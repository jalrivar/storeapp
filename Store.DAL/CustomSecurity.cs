﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.DAL
{
    public class CustomSecurity : Singleton<CustomSecurity>
    {
        private CustomSecurity() { }

        public IEnumerable<Models.Response> SpValidateUser(string userName, string password)
        {
            using (var db = new SecurityStoreEntities())
            {
                return db.Database.SqlQuery<Models.Response>("EXEC SpValidateUser @Username, @Password"
                    , new SqlParameter("@UserName", userName)
                    , new SqlParameter("@Password", password)).ToList();
            }
        }
    }
}
