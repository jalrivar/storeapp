﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.DAL.Models
{
    public class Response
    {
        public int RESULT_CODE { set; get; }
        public string RESULT_MESSAGE { set; get; }
    }
}
