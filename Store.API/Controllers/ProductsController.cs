﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using Newtonsoft.Json;
using Store.API.Models;
using Store.DAL;

namespace Store.API.Controllers
{
    public class ProductsController : ApiController
    {
        private Entities db = new Entities();

        // GET: api/Products/5
        [ResponseType(typeof(Products))]
        public IHttpActionResult GetProducts(int id)
        {
            Products products = db.Products.Find(id);
            if (products == null)
            {
                return NotFound();
            }

            return Ok(products);
        }

        // GET: api/Products
        [AllowAnonymous]
        public IEnumerable<Products> GetProducts([FromUri]PagingParameterModel pagingParameterModel)
        {
            var source = (from customer in db.Products.
                            OrderBy(a => a.ProductName)
                          select customer).AsQueryable();

            source = source.ApplySort(pagingParameterModel.sort);

            if (!string.IsNullOrEmpty(pagingParameterModel.QuerySearch))
                source = source.Where(a => a.ProductName.Contains(pagingParameterModel.QuerySearch));

            var count = source.Count();
            var currentPage = pagingParameterModel.pageNumber;
            var pageSize = pagingParameterModel.pageSize;
            var totalCount = count;
            var totalPages = (int)Math.Ceiling(count / (double)pageSize);
            var items = source.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            var previousPage = currentPage > 1 ? "Yes" : "No";
            var nextPage = currentPage < totalPages ? "Yes" : "No";
            var paginationMetadata = new
            {
                totalCount,
                pageSize,
                currentPage,
                totalPages,
                previousPage,
                nextPage,
                QuerySearch = string.IsNullOrEmpty(pagingParameterModel.QuerySearch) ?
                              "No Parameter Passed" : pagingParameterModel.QuerySearch
            };

            HttpContext.Current.Response.Headers.Add("Paging-Headers", JsonConvert.SerializeObject(paginationMetadata));

            return items;
        }

        // PUT: api/Products/PutProducts/5
        [Authorize(Roles = "Admin")]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutProducts(int id, Products products)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != products.ProductId)
            {
                return BadRequest();
            }

            db.Entry(products).State = EntityState.Modified;

            try
            {
                db.SaveChanges();

                var movement = new Movements { ProductId =  products.ProductId, SalePrice = products.SalePrice, CostPrice = products.CostPrice, MovementDate = DateTime.Now, MovementName = "ProductUpdate", Stock = products.Stock};
                db.Movements.Add(movement);
                db.SaveChanges();

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductsExists(id))
                {
                    return NotFound();
                }

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Products
        [Authorize(Roles = "Admin")]
        [ResponseType(typeof(Products))]
        public IHttpActionResult PostProducts(Products products)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Products.Add(products);
            db.SaveChanges();

            var movement = new Movements { ProductId = products.ProductId, SalePrice = products.SalePrice, CostPrice = products.CostPrice, MovementDate = DateTime.Now, MovementName = "ProductCreate", Stock = products.Stock };
            db.Movements.Add(movement);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = products.ProductId }, products);
        }

        // DELETE: api/Products/5
        [Authorize(Roles = "Admin")]
        [ResponseType(typeof(Products))]
        public IHttpActionResult DeleteProducts(int id)
        {
            Products products = db.Products.Find(id);
            if (products == null)
            {
                return NotFound();
            }

            db.Products.Remove(products);
            db.SaveChanges();

            return Ok(products);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public bool ProductsExists(int id)
        {
            return db.Products.Count(e => e.ProductId == id) > 0;
        }

        public void AffectStock(int id, decimal soldQuantity)
        {
            var product = db.Products.Find(id);
            if (product == null) return;
            product.Stock = product.Stock - soldQuantity;
            db.Entry(product).State = EntityState.Modified;
            db.SaveChanges();
        }
    }

    public static class QueryableApplySortExtension
    {
        public static IQueryable<T> ApplySort<T>(this IQueryable<T> source, string strSort)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source), "Data source is empty.");
            }

            if (strSort == null)
            {
                return source;
            }

            var lstSort = strSort.Split(',');

            var sortExpression = string.Empty;

            foreach (var sortOption in lstSort)
            {
                if (sortOption.StartsWith("-"))
                {
                    sortExpression = sortExpression + sortOption.Remove(0, 1) + " descending,";
                }
                else
                {
                    sortExpression = sortExpression + sortOption + ",";
                }
            }

            if (!string.IsNullOrWhiteSpace(sortExpression))
                source = source.OrderBy(sortExpression.Remove(sortExpression.Count() - 1));

            return source;
        }
    }
}