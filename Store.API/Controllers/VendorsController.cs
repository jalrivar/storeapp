﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Store.DAL;

namespace Store.API.Controllers
{
    public class VendorsController : ApiController
    {
        private Entities db = new Entities();

        // GET: api/Vendors
        public IQueryable<Vendors> GetVendors()
        {
            return db.Vendors;
        }

        // GET: api/Vendors/5
        [ResponseType(typeof(Vendors))]
        public IHttpActionResult GetVendors(int id)
        {
            Vendors vendors = db.Vendors.Find(id);
            if (vendors == null)
            {
                return NotFound();
            }

            return Ok(vendors);
        }

        // PUT: api/Vendors/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutVendors(int id, Vendors vendors)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vendors.VendorId)
            {
                return BadRequest();
            }

            db.Entry(vendors).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VendorsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Vendors
        [ResponseType(typeof(Vendors))]
        public IHttpActionResult PostVendors(Vendors vendors)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Vendors.Add(vendors);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = vendors.VendorId }, vendors);
        }

        // DELETE: api/Vendors/5
        [ResponseType(typeof(Vendors))]
        public IHttpActionResult DeleteVendors(int id)
        {
            Vendors vendors = db.Vendors.Find(id);
            if (vendors == null)
            {
                return NotFound();
            }

            db.Vendors.Remove(vendors);
            db.SaveChanges();

            return Ok(vendors);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VendorsExists(int id)
        {
            return db.Vendors.Count(e => e.VendorId == id) > 0;
        }
    }
}