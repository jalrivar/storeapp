﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Store.DAL;

namespace Store.API.Controllers
{
    public class LikesProductsController : ApiController
    {
        private Entities db = new Entities();

        // PUT: api/Likes/5
        [ResponseType(typeof(void))]

        public IHttpActionResult Put(int id)
        {
            var products = db.Products.Find(id);
            if (products == null)
            {
                return NotFound();
            }

            products.Likes = products.Likes + 1;

            db.Entry(products).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!new ProductsController().ProductsExists(id))
                {
                    return NotFound();
                }

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}
