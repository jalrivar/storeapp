﻿using Store.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Store.API
{
    public class CredentialChecker
    {
        public User CheckCredential(string userName, string password)
        {
            var resp = new DAL.User();
            using (var db = new SecurityStoreEntities())
            {
                if (CustomSecurity.Instance.SpValidateUser(userName, password).First().RESULT_CODE.Equals(0))
                    resp = db.User.First(x => x.Username == userName);
            }
            return resp;
        }
    }

    public class AuthenticationHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            try
            {
                request.Headers.TryGetValues("Authorization", out IEnumerable<string> values);
                var tokens = values?.FirstOrDefault();
                //var tokens = request.Headers.GetValues("Authorization").FirstOrDefault();
                if (tokens != null)
                {
                    byte[] data = Convert.FromBase64String(tokens);
                    string decodedString = Encoding.UTF8.GetString(data);
                    string[] tokensValues = decodedString.Split(':');

                    DAL.User ObjUser = new CredentialChecker().CheckCredential(tokensValues[0], tokensValues[1]);
                    if (ObjUser != null)
                    {
                        IPrincipal principal = new GenericPrincipal(new GenericIdentity(ObjUser.Username), ObjUser.UserRole.Split(','));
                        Thread.CurrentPrincipal = principal;
                        HttpContext.Current.User = principal;
                    }
                    else
                    {
                        //The user is unauthorize and return 401 status  
                        var response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                        var tsc = new TaskCompletionSource<HttpResponseMessage>();
                        tsc.SetResult(response);
                        return tsc.Task;
                    }
                }
                else
                {
                    //Otorgando acceso sin autorización.
                    if (request.RequestUri.AbsolutePath.Equals(@"/API/Products", StringComparison.OrdinalIgnoreCase))
                    {
                        return base.SendAsync(request, cancellationToken);
                    }

                    //Bad Request request because Authentication header is set but value is null  
                    var response = new HttpResponseMessage(HttpStatusCode.Forbidden);
                    var tsc = new TaskCompletionSource<HttpResponseMessage>();
                    tsc.SetResult(response);
                    return tsc.Task;
                }
                return base.SendAsync(request, cancellationToken);
            }
            catch
            {
                //User did not set Authentication header  
                var response = new HttpResponseMessage(HttpStatusCode.Forbidden);
                var tsc = new TaskCompletionSource<HttpResponseMessage>();
                tsc.SetResult(response);
                return tsc.Task;
            }
        }

    }
}